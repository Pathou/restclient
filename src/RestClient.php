<?php
namespace Carbone\RestClient;

/**
 * Classe de client REST
 */
class RestClient {

  private $_base_url;
  
  private $content;
  private $headers;
  
  public function __construct($base_url = '') {
    $this->_base_url = $base_url;
  }

  /**
   * Ajoute l'url au client
   * @param string $base_url
   * @return \RestClient
   */
  public function setUrl($base_url) {
    $this->_base_url = $base_url;
    return $this;
  }

  /**
   * Retourne le content
   * @return string
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * Retourne les headers
   * @return array
   */
  public function getHeaders() {
    return $this->headers;
  }

  /**
   * Appel GET
   * @param string $url
   * @param array $pParams
   * @return array
   */
  public function get($url = '', $pParams = array()) {
    return $this->_launch($this->_makeUrl($url, $pParams), $this->_createContext('GET'));
  }
  
  /**
   * Affiche uniquement le content
   * @return string
   */
  public function __toString() {
    return $this->content;
  }

  /**
   * Appel POST
   * @param string $url
   * @param array $pPostParams
   * @param array $pGetParams
   * @return array
   */
  public function post($url = '', $pPostParams = array(), $pGetParams = array()) {
    return $this->_launch($this->_makeUrl($url, $pGetParams), $this->_createContext('POST', $pPostParams));
  }


  /**
   * Appel PUT
   * @param string $url
   * @param array $pContent
   * @param array $pGetParams
   * @return array
   */
  public function put($url = '', $pContent = null, $pGetParams = array()) {
    return $this->_launch($this->_makeUrl($url, $pGetParams), $this->_createContext('PUT', $pContent));
  }


  /**
   * Appel DELETE
   * @param string $url
   * @param array $pContent
   * @param array $pGetParams
   * @return array
   */
  public function delete($url = '', $pContent = null, $pGetParams = array()) {
    return $this->_launch($this->_makeUrl($url, $pGetParams), $this->_createContext('DELETE', $pContent));
  }

  protected function _createContext($pMethod, $pContent = null) {
    $opts = array(
        'http' => array(
            'method' => $pMethod,
            'header' => 'Content-type: application/x-www-form-urlencoded',
        )
    );
    if ($pContent !== null) {
      if (is_array($pContent)) {
        $pContent = http_build_query($pContent);
      }
      $opts['http']['content'] = $pContent;
    }
    return stream_context_create($opts);
  }

  protected function _makeUrl($suffix_url, $pParams) {
    $final_url = $this->_base_url . $suffix_url;
    return $final_url
            . (strpos($final_url, '?') ? '' : '?')
            . http_build_query($pParams);
  }

  protected function _launch($final_url, $context) {
    if (($stream = fopen($final_url, 'r', false, $context)) !== false) {
      $this->content = stream_get_contents($stream);
      $this->headers = stream_get_meta_data($stream);
      fclose($stream);
      return $this;
    } else {
      throw new \Exception('Impossible Connection');
    }
  }

}
